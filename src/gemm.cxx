#include "gemm.h"
using namespace upcxx;
using namespace std;

void DArray_DGEMM( char transa, char transb, int m, int n, int k, \
                   double alpha, DArray<double,2> &A, DArray<double,2> &B, \
                   double beta, DArray<double,2> &C ) {
#ifdef DEBUG
  if (myrank() == 0) cout << "A:" << endl; 
  A.print();
  if (myrank() == 0) cout << "B:" << endl; 
  B.print();
  if (myrank() == 0) cout << "C:" << endl; 
  C.print();
#endif

  //if (transa == 'T' or transa == 't') {
  //  //TODO?:
  //  //A.transpose();
  //} else if (transa != 'N' and transa != 'n') {
  //}

  //if (transb == 'T' or transb == 't') {
  //  //TODO?:
  //  //B.transpose();
  //} else if (transb != 'N' and transb != 'n') {
  //}


  /* TODO: good version with tiling
  myAblock = A.get_local_block();
  for (int i=0; i<nprows; i++) 
    for (int j=0; j<npcols; j++) 
      ...
  */

  /* Stupid version: */
  if ( myrank() == 0 ) {

    double *localA = (double *) malloc ( sizeof(double) * m * k );
    double *localB = (double *) malloc ( sizeof(double) * k * n );
    double *localC = (double *) malloc ( sizeof(double) * m * n );

    A.get_storage( A.get_global_low(), A.get_global_high(), &localA );
    B.get_storage( B.get_global_low(), B.get_global_high(), &localB );
    C.get_storage( C.get_global_low(), C.get_global_high(), &localC );

    int lda = m;
    int ldc = m;
    int ldb = m;
    dgemm_( &transa, &transb, &m, &n, &k, &alpha, localA, &lda, localB, &ldb, &beta, localC, &ldc );

#ifdef DEBUG
    for (int i=0; i<m; i++) {
      for (int j=0; j<n; j++) {
        cout << localC[i*n + j] << " ";
      } cout << endl;
    }
#endif

    C.put_storage( C.get_global_low(), C.get_global_high(), localC );

  }
  barrier();

  return;
}
