#ifndef __ADD_H__
#define __ADD_H__

#include "darray.h"

void DArray_add_matching( double const &, DArray<double, 2> &, double const &, \
                          DArray<double, 2> &, DArray<double, 2> & );

#endif
