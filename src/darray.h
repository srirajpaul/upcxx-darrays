#ifndef DARRAY_H
#define DARRAY_H

#include <upcxx.h>

//#define UPCXXA_DEFAULT_CMAJOR 

#include <upcxx/array.h>
#include <tuple>
#include <functional>
#include <iomanip>
#include <string>
using namespace upcxx;
using namespace std;

#define COL 1
#define ROW 0

//#define DEBUG

template<typename T, int ndim>
class DArray {

  private:

    int rows, cols, row_elems, col_elems, tot_elems;
    int elems_per_row, elems_per_col;
    double ave_elems_per_row, ave_elems_per_col;
    int myrow, mycol;
    point<ndim> lowpt, highpt, global_lo, global_hi;
    rectdomain<ndim> rd, global_rd;
    ndarray<T, ndim> local_array; 
    //shared_array<ndarray<T, ndim, global>, 1> GlobalArray;
    ndarray<ndarray<T, ndim, global>, 1> GlobalArray;
    /* TODO: Instead, I think this should probably be replicated with exchange(): */
    //shared_array<tuple<int, int>, 1> RowsCols;
    ndarray<tuple<int, int>, 1> RowsCols;
    bool irregular;
    ndarray<rectdomain<ndim>, 1> RowsCols_coords;

    /* get_owner_ranks(): Find the ranks of the processes containing the tile */
    /* params:
         point<ndim> p0 : upper left corner                    (input)
         point<ndim> p1 : lower right corner                   (input)
         vector<int> owners : vector containing rank owners    (output)       */
    void get_owner_ranks(point<ndim> const p0, point<ndim> const p1, \
                         vector<int>& owners) {

      /* find ranks that own the two corners: */
      int row0 = p0.x[0] / elems_per_row;
      int col0 = p0.x[1] / elems_per_col;
      int row1 = p1.x[0] / elems_per_row;
      int col1 = p1.x[1] / elems_per_col;
      int rank0 = row0 * cols + col0;
      int rank1 = row1 * cols + col1;

#ifdef DEBUG
      cout << "ROW0:" << row0 << endl;
      cout << "COL0:" << col0 << endl;
      cout << "RANK0:" << rank0 << endl;
      cout << "ROW1:" << row1 << endl;
      cout << "COL1:" << col1 << endl;
      cout << "RANK1:" << rank1 << endl;
#endif

      int height = row1 - row0;
      int width  = col1 - col0;

      int l = rank0;
      int nrows = height+1;
      int ncolumns = width+1;

      for (int j=0; j<nrows; j++) {
        for (int i=l; i<l+ncolumns; i++) {
#ifdef DEBUG
          cout << i << ", ";
#endif
          owners.push_back(i);
        }
        cout << endl;
        l += cols;
      }

      return;
    }

    /* get_owner_ranks_irreg(): Find the ranks of the procs containing a tile */
    /* params:
         point<ndim> p0 : upper left corner                    (input)
         point<ndim> p1 : lower right corner                   (input)
         vector<int> owners : vector containing rank owners    (output)       */
    void get_owner_ranks_irreg(point<ndim> const p0, point<ndim> const p1, \
                         vector<int>& owners) {

      /* guess ranks that own the two corners based on regular distribution: */
      int row0_guess = p0.x[0] / ave_elems_per_row;
      int col0_guess = p0.x[1] / ave_elems_per_col;
      int row1_guess, col1_guess;
      if (p1.x[0] == row_elems) {
        row1_guess = rows-1;
      }
      else {
        row1_guess = p1.x[0] / ave_elems_per_row;
      }
      if (p1.x[1] == col_elems) {
        col1_guess = cols-1;
      }
      else {
        col1_guess = p1.x[1] / ave_elems_per_col;
      }
      bool searching_rows = true;
      bool searching_cols = true;

#ifdef DEBUG
      cout << "Looking for " << p0 << ", " << p1 << " within:" << endl;
      for (int i=0; i<ranks(); i++) {
        cout << "RCc["<<i<<"]: " << RowsCols_coords[PT(i)] << endl;
        cout << "RCc["<<i<<"].min.[0]: "<<RowsCols_coords[PT(i)].min().x[0]<<endl;
      }
#endif

      int guess_row_low, guess_row_high, guess_col_low, guess_col_high;
      while ( searching_rows == true || searching_cols == true ) {

#ifdef DEBUG
        cout << "row0_guess: " << row0_guess << endl;
        cout << "col0_guess: " << col0_guess << endl;
#endif

        if ( searching_rows ) {
          guess_row_low  = RowsCols_coords[PT(row0_guess*cols)].min().x[0];
          guess_row_high = (RowsCols_coords[PT(row0_guess*cols)].max()).x[0];
#ifdef DEBUG
          cout << "guess_row_low:" << guess_row_low << endl;
          cout << "guess_row_high:" << guess_row_high << endl;
#endif
          if (p0.x[0] < guess_row_low) {
            row0_guess--;
#ifdef DEBUG
            cout << "guess less row 0:" << row0_guess << endl;
#endif
          } else if ( p0.x[0] > guess_row_high ) {
            row0_guess++;
#ifdef DEBUG
            cout << "guess more row 0:" << row0_guess << endl;
#endif
          } else { searching_rows = false; }
        }

        if ( searching_cols ) {
          guess_col_low  = RowsCols_coords[PT(col0_guess)].min().x[1];
          guess_col_high = (RowsCols_coords[PT(col0_guess)].max()).x[1];
#ifdef DEBUG
          cout << "guess_col_low:" << guess_col_low << endl;
          cout << "guess_col_high:" << guess_col_high << endl;
#endif
          if (p0.x[1] < guess_col_low) {
            col0_guess--;
#ifdef DEBUG
            cout << "guess less col 0:" << col0_guess << endl;
#endif
          } else if ( p0.x[1] > guess_col_high ) {
            col0_guess++;
#ifdef DEBUG
            cout << "guess more col 0:" << col0_guess << endl;
#endif
          } else { searching_cols = false; }
        }
      }
#ifdef DEBUG
      cout << "final guess: row=" << row0_guess << ", " \
                        << "col=" << col0_guess << endl;
      cout << "In other words, rank:" << row0_guess * cols + col0_guess << endl;
#endif


      /* TODO: this should probably be a function (similar loop above) */
      searching_rows = true;
      searching_cols = true;
      while ( searching_rows == true || searching_cols == true ) {

#ifdef DEBUG
        cout << "row1_guess: " << row1_guess << endl;
        cout << "col1_guess: " << col1_guess << endl;
#endif

        if ( searching_rows ) {
          guess_row_low  = RowsCols_coords[PT(row1_guess*cols)].min().x[0];
          guess_row_high = (RowsCols_coords[PT(row1_guess*cols)].max()).x[0];
#ifdef DEBUG
          cout << "guess_row_low:" << guess_row_low << endl;
          cout << "guess_row_high:" << guess_row_high << endl;
#endif
          if (p1.x[0] < guess_row_low) {
            row1_guess--;
#ifdef DEBUG
            cout << "guess less row 0:" << row1_guess << endl;
#endif
          } else if ( p1.x[0] > guess_row_high ) {
              if ( p1.x[0] == row_elems ) {
                searching_rows = false;
#ifdef DEBUG
                cout << "Row edge case!" << endl;
#endif
              } else {
                row1_guess++;
#ifdef DEBUG
                cout << "guess more row 0:" << row1_guess << endl;
#endif
              }
          } else { searching_rows = false; }
        }

        if ( searching_cols ) {
          guess_col_low  = RowsCols_coords[PT(col1_guess)].min().x[1];
          guess_col_high = (RowsCols_coords[PT(col1_guess)].max()).x[1];
#ifdef DEBUG
          cout << "guess_col_low:" << guess_col_low << endl;
          cout << "guess_col_high:" << guess_col_high << endl;
#endif
          if (p1.x[1] < guess_col_low) {
            col1_guess--;
#ifdef DEBUG
            cout << "guess less col 0:" << col1_guess << endl;
#endif
          } else if ( p1.x[1] > guess_col_high ) {
              if (p1.x[1] == col_elems) {
                searching_cols = false;
#ifdef DEBUG
                cout << "Col edge case!" << endl;
#endif
              } else {
                col1_guess++;
#ifdef DEBUG
                cout << "guess more col 0:" << col0_guess << endl;
#endif
              }
          } else { searching_cols = false; }
        }
      }

#ifdef DEBUG
      cout << "final guess: row=" << row1_guess << ", " \
                        << "col=" << col1_guess << endl;
      cout << "In other words, rank:" << row1_guess * cols + col1_guess << endl;
#endif

      int rank0 = row0_guess * cols + col0_guess;
      int rank1 = row1_guess * cols + col1_guess;

      int height = row1_guess - row0_guess;
      int width  = col1_guess - col0_guess;

      int l = rank0;
      int nrows = height+1;
      int ncolumns = width+1;

#ifdef DEBUG
      cout << "height: " << height << endl;
      cout << "width: " << width << endl;
#endif

      for (int j=0; j<nrows; j++) {
        for (int i=l; i<l+ncolumns; i++) {
#ifdef DEBUG
          cout << i << ", ";
#endif
          owners.push_back(i);
        }
#ifdef DEBUG
        cout << endl;
#endif
        l += cols;
      }

      return;
    }

    bool domain_check (rectdomain<2> rd) {
      int dim0len = rd.max().x[0] - rd.min().x[0];
      int dim1len = rd.max().x[1] - rd.min().x[1];
      if ( dim0len == -1 and dim1len == -1 ) {
        return false;
      } else
          return true;
    }

  public:

    DArray(int rws, int cls, int elems) {

      if ( ranks() != rws*cls ) {
        if (myrank() == 0)
          cerr << "ERROR: Number of ranks should equal rows * cols (for now).\n";
        finalize();
        exit(EXIT_FAILURE);
      }

      irregular = false;
      rows = rws;
      cols = cls;
      row_elems = col_elems = elems;
      tot_elems = elems*elems;
      global_lo = PT(0,0);
      global_hi = PT(elems, elems);
      global_rd = RD( global_lo, global_hi );
      elems_per_row = elems/rws;
      elems_per_col = elems/cls;
      ave_elems_per_row = (double)elems/rws;
      ave_elems_per_col = (double)elems/cls;
      myrow = myrank() / cls;
      mycol = myrank() % cls;
      lowpt = PT( elems_per_row*myrow, elems_per_col*mycol );
      highpt = PT( elems_per_row*(myrow+1), elems_per_col*(mycol+1) );
      rd = RD( lowpt, highpt );
      local_array.create( rd ); 
      highpt = highpt - PT( 1, 1 );

#ifdef DEBUG
      if (myrank() == 2 ) {
        cout << "lo = " << lowpt << endl;
        cout << "hi = " << highpt << endl;
        cout << "rd = " << rd << endl;
        cout << "myrow, mycol = " << myrow << ", " << mycol << endl;
      }
#endif

      upcxx_foreach(pt, rd) {
        local_array[pt] = 0.0;
        //local_array[pt] = (double)pt.x[0]+(double)pt.x[1];
        //if (myrank() == 2 ) cout << "pt = " << pt << endl;
      };

      //GlobalArray.init( ranks() );
      //GlobalArray[ myrank() ] = local_array;
      GlobalArray.create( RD( (int) ranks() ) );
      GlobalArray.exchange( local_array );

      //RowsCols.init( ranks() );
      //RowsCols[ myrank() ] = make_tuple( myrow, mycol );
      RowsCols.create( RD( (int) ranks() ) );
      RowsCols.exchange( make_tuple( myrow, mycol ) );

      barrier();

    }

    DArray(int rws, int cls, rectdomain<ndim> globalrd, ndarray<T, ndim> local_block) {

      if ( ranks() != rws*cls ) {
        if (myrank() == 0)
          cerr << "ERROR: Number of ranks should equal rows * cols (for now).\n";
        finalize();
        exit(EXIT_FAILURE);
      }

      irregular = true;
      rows = rws;
      cols = cls;

      global_rd = globalrd;
      global_lo = PT( global_rd.min().x[0], global_rd.min().x[1] );
      global_hi = PT( global_rd.max().x[0], global_rd.max().x[1] ) + PT(1,1);
#ifdef DEBUG
      cout << "global_lo: " << global_lo << endl;
      cout << "global_hi: " << global_hi << endl;
#endif

      row_elems = (global_rd.max()+PT(1,1)).x[0] - global_rd.min().x[0];
      col_elems = (global_rd.max()+PT(1,1)).x[1] - global_rd.min().x[1];
      tot_elems = row_elems * col_elems;
      elems_per_row = row_elems/rws;
      elems_per_col = col_elems/cls;
      ave_elems_per_row = (double)row_elems/rws;
      ave_elems_per_col = (double)col_elems/cls;
      myrow = myrank() / cls;
      mycol = myrank() % cls;
      lowpt = local_block.domain().min();
      highpt = local_block.domain().max();
      /* TODO: don't copy local_block when you don't have to... */
      rd = RD( lowpt, highpt+PT(1,1) );
      local_array.create( rd ); 
      //highpt = highpt - PT( 1, 1 );

#ifdef DEBUG
      if (myrank() == 2 ) {
        cout << "lo = " << lowpt << endl;
        cout << "hi = " << highpt << endl;
        cout << "rd = " << rd << endl;
        cout << "myrow, mycol = " << myrow << ", " << mycol << endl;
      }
#endif

      upcxx_foreach(pt, rd) {
        //local_array[pt] = 0.0;
        local_array[pt] = local_block[pt];
        //local_array[pt] = (double)pt.x[0]+(double)pt.x[1];
        //if (myrank() == 2 ) cout << "pt = " << pt << endl;
      };

      //GlobalArray.init( ranks() );
      //GlobalArray[ myrank() ] = local_array;
      GlobalArray.create( RD( (int) ranks() ) );
      GlobalArray.exchange( local_array );

      //RowsCols.init( ranks() );
      //RowsCols[ myrank() ] = make_tuple( myrow, mycol );
      RowsCols.create( RD( (int) ranks() ) );
      RowsCols.exchange( make_tuple( myrow, mycol ) );

      RowsCols_coords.create( RD( (int)ranks() ) );
      RowsCols_coords.exchange( rd );

      barrier();

    }

    /* Custom copy constructor */
    //void DArray_duplicate( const DArray& D ) {
    DArray( const DArray& D ) {
      
      rows = D.rows;
      cols = D.cols;
      row_elems = D.row_elems;
      col_elems = D.col_elems;
      tot_elems = D.tot_elems;
      elems_per_row = D.elems_per_row;
      elems_per_col = D.elems_per_col;
      ave_elems_per_row = D.ave_elems_per_row;
      ave_elems_per_col = D.ave_elems_per_col;
      myrow = D.myrow;
      mycol = D.mycol;
      lowpt = D.lowpt;
      highpt = D.highpt;
      global_lo = D.global_lo;
      global_hi = D.global_hi;
      rd = D.rd;
      global_rd = D.global_rd;
      irregular = D.irregular;
      RowsCols.create( RD( (int) ranks() ) );
      RowsCols.exchange( make_tuple( myrow, mycol ) );
      RowsCols_coords.create( RD( (int)ranks() ) );
      RowsCols_coords.exchange( rd );
      local_array.create( rd ); 
      local_array.copy(D.local_array);
      GlobalArray.create( RD( (int) ranks() ) );
      GlobalArray.exchange( local_array );
      barrier();

    }

    /* Custom destructor */
    //~DArray() {
    //  RowsCols.destroy();
    //  RowsCols_coords.destroy();
    //  local_array.destroy(); 
    //  GlobalArray.destroy();
    //}

    int get_elems_per_row() const { return elems_per_row; }

    int get_elems_per_col() const { return elems_per_col; }

    int get_myrow() const { return myrow; }

    int get_mycol() const { return mycol; }

    point<ndim> get_lowpt() const { return lowpt; }

    point<ndim> get_highpt() const { return highpt; }

    point<ndim> get_global_low() const { return global_lo; }
    point<ndim> get_global_high() const { return global_hi; }

    ndarray<T, ndim>& get_local_block() { return local_array; }

    T * get_local_block_ptr() { return local_array.storage_ptr(); }

    void distribution( point<ndim> &lo, point<ndim> &hi ) const { 
      lo = lowpt;
      hi = highpt;
      return;
    }

    void update() { GlobalArray.exchange( local_array ); barrier(); }


    /* This function gets a tile in a "fine-grained" fashion, in which 
       each contiguous row for partial tiles within each process has a 
       correponding async_copy call */
    ndarray<T, ndim> get_tile_fg(point<ndim> const &p0, point<ndim> const &p1) {

      /* TODO: Error checking:
                  -> 'upper left' to 'lower right' tiles only 
                  -> 
      */

      vector<int> owner_ranks;
      int ncolumns, this_column = 0;
      int nrows;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks, ncolumns, nrows);
      else
        get_owner_ranks(p0, p1, owner_ranks, ncolumns, nrows);

#ifdef DEBUG
      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++)
        cout << "owner : " << *it << endl;
        cout << "nrows : " << nrows << endl;
        cout << "ncolumns : " << ncolumns << endl;
#endif

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      point<ndim> start = p0;

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {

#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
        cout << "this_col : " << get<COL>(RowsCols[*it]/*.get()*/) << endl;
        cout << "this_start_pt : " << start.x[COL] << endl;
#endif

        unsigned int this_col_endpt;
        if ( irregular )
            this_col_endpt = (RowsCols_coords[PT(*it)].max()).x[COL] + 1;
        else
            this_col_endpt = ( get<COL>( RowsCols[*it]/*.get()*/)+1 ) * elems_per_col;
#ifdef DEBUG
        cout << "this_col_endpt : " << this_col_endpt << endl;
#endif
        unsigned int nelems;
        if (p1.x[COL] < this_col_endpt )
          nelems = p1.x[COL] - start.x[COL];
        else
          nelems = this_col_endpt - start.x[COL];
#ifdef DEBUG
        cout << "nelems : " << nelems << endl;
        cout << "this_row: " << get<ROW>(RowsCols[*it]/*.get()*/) << endl;
        cout << "this_start_pt : " << start.x[ROW] << endl;
#endif
        unsigned int this_row_endpt;
        if ( irregular )
            this_row_endpt = (RowsCols_coords[PT(*it)].max()).x[ROW] + 1;
        else 
            this_row_endpt = ( get<ROW>(RowsCols[*it]/*.get()*/)+1 ) * elems_per_row;
#ifdef DEBUG
        cout << "this_row_endpt : " << this_row_endpt << endl;
#endif

        unsigned int ntilerows;
        if (p1.x[ROW] < this_row_endpt )
          ntilerows = p1.x[ROW] - start.x[ROW];
        else
          ntilerows = this_row_endpt - start.x[ROW];
#ifdef DEBUG
        cout << "ntilerows : " << ntilerows << endl;
#endif

        /* async_copy all rows for this process into local array */
        point<ndim> mystart = start;
        for (int i=0; i<ntilerows; i++) {
          async_copy(&(GlobalArray[*it]/*.get()*/[mystart]), 
                     (global_ptr<T>)&(mytile[mystart]), nelems);
          mystart += PT(1,0);
        }

#ifdef DEBUG
        cout << "(*it+1) (mod) cols = " << (*it+1) % cols << endl;
        cout << "(*it+1) = " << (*it+1) << endl;
        cout << "cols = " << cols << endl;
        cout << "(this_column+1) (mod) ncolumns = " << (this_column+1) % ncolumns << endl;
        cout << "(this_column) = " << this_column << endl;
        cout << "ncolumns= " << ncolumns << endl;
#endif

        if ( (this_column+1) % ncolumns == 0 ) {
#ifdef DEBUG
          cout << "new row" << endl;
#endif
          start.x[COL] = p0.x[COL];
          start.x[ROW] = start.x[ROW] + ntilerows;
          this_column = 0;
        } else {
          start.x[1] += nelems;
          this_column++;
        }

      }
      async_copy_fence();

#ifdef DEBUG
      cout << "DELEMS:";
      upcxx_foreach(pt, RD(p0, p1) ) {
        if (( pt.x[1]-(p0.x[1]) ) % (p1.x[1] - p0.x[1]) == 0) cout << endl;
        cout << mytile[pt] << ", ";
      };
      cout << endl << endl;
#endif

      return mytile;

    }


    ndarray<T, ndim> operator()(point<ndim> const &p0, point<ndim> const &p1) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        mytile.async_copy(GlobalArray[*it]/*.get()*/);
      }
      async_wait();

      return mytile;
    }

    void operator()(point<ndim> const &p0, point<ndim> const &p1, \
                    ndarray<T, ndim> &mytile) {

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        mytile.async_copy(GlobalArray[*it]/*.get()*/);
      }
      async_wait();

      return;
    }

    void get_storage(point<ndim> const &p0, point<ndim> const &p1, \
                     T **myarray) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        mytile.async_copy(GlobalArray[*it]/*.get()*/);
#ifdef DEBUG
        cout << "copied." << endl;
#endif
      }
#ifdef DEBUG
      cout << "waiting..." << endl;
#endif
      async_wait();
#ifdef DEBUG
      cout << "out." << endl;
#endif

      *myarray = mytile.storage_ptr();

      return;
    }

    void get_storage(point<ndim> const &p0, point<ndim> const &p1, T *myarray) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        mytile.async_copy(GlobalArray[*it]/*.get()*/);
      }
      async_wait();
      
      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      memcpy(myarray, mytile.storage_ptr(), sizeof(T)*dim1*dim2 );

      return;
    }

    void get_storage_ld(point<ndim> const &p0, point<ndim> const &p1, T *myarray, int const &ld) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        mytile.async_copy(GlobalArray[*it]/*.get()*/);
      }
      async_wait();

#ifdef DEBUG
      cout << "DELEMS:";
      upcxx_foreach(pt, RD(p0, p1) ) {
        if (( pt.x[1]-(p0.x[1]) ) % (p1.x[1] - p0.x[1]) == 0) cout << endl;
        cout << mytile[pt] << ", ";
      };
      cout << endl << endl;
#endif

      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      for (int i=0; i<dim1; i++)
        memcpy(myarray+i*ld, mytile.storage_ptr()+i*dim2, sizeof(double)*dim2);

      return;
    }

    void get_tile(ndarray<T, ndim> *tile) {

      point<ndim> p0 = tile->domain().min();
      point<ndim> p1 = tile->domain().max();

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        tile->async_copy(GlobalArray[*it].get());
      }
      async_wait();

      return;
    }


    void put_tile(ndarray<T, ndim> const &tile) {

#ifdef DEBUG
      cout << "domain: " << tile.domain() << endl;
      cout << "max: " << tile.domain().max() << endl;
      cout << "minm: " << tile.domain().min() << endl;
#endif

      point<ndim> p0 = tile.domain().min();
      point<ndim> p1 = tile.domain().max();

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        (GlobalArray[*it]/*.get()*/).async_copy(tile);
      }
      async_wait();
      return;

    }

    void put_tile(point<ndim> const &p0, point<ndim> const &p1, T const *myarray, int ncols=0, bool ld_col=false) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
#ifdef DEBUG
      cout << "dim1: " << dim1 << endl;
      cout << "dim2: " << dim2 << endl;
#endif

      //if (ld_col) {
      //  int i = 0;
      //  int row = 0;
      //  cout << "mya:" << endl;
      //  upcxx_foreach( pt, RD(p0, p1) ) {
      //    //mytile[pt] = myarray[pt.x[1]*ncols + pt.x[0]];
      //    cout << myarray[i] << ", ";
      //    mytile[PT(pt.x[0],pt.x[1])] = myarray[i++];
      //    if (i%dim2== 0) {
      //      cout << "SKIPPING" << endl;
      //      row+2;  // WTF?
      //      i = ncols*row;
      //    }
      //  };
      //} else 
            memcpy(mytile.storage_ptr(), myarray, sizeof(T)*dim1*dim2 );
            //TODO: check that's row_major with .is_row(); 

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
#ifdef DEBUG
        cout << "this_rank : " << *it << endl;
#endif
        (GlobalArray[*it]/*.get()*/).async_copy(mytile);
      }
      async_wait();
      return;

    }

    void put_storage(point<ndim> const &p0, point<ndim> const &p1, T const *myarray) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      // TODO: ?
      //mytile.storage_ptr() = myarray;

      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      memcpy(mytile.storage_ptr(), myarray, sizeof(T)*dim1*dim2 );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
        (GlobalArray[*it]/*.get()*/).async_copy(mytile);
      }
      async_wait();

      return;
    }

    void put_storage_ld(point<ndim> const &p0, point<ndim> const &p1, T const *myarray, int const &ld) {

      ndarray<T, ndim> mytile;
      mytile.create( RD(p0, p1) );

      // TODO: ?
      //mytile.storage_ptr() = myarray;

      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      for (int i=0; i<dim1; i++)
        memcpy(mytile.storage_ptr()+i*dim2, &(myarray[i*ld]), sizeof(T)*dim2 );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
        (GlobalArray[*it]/*.get()*/).async_copy(mytile);
      }
      async_wait();

      return;
    }

    /* Warning: don't use this */
    void acc_storage(point<ndim> const &p0, point<ndim> const &p1, \
                     T const *myarray, T const &alpha) {

      ndarray<T, ndim> mytile, remote_tile;
      mytile.create( RD(p0, p1) );
      remote_tile.create( RD(p0, p1) );

      // TODO: ?
      //mytile.storage_ptr() = myarray;

      int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      memcpy(mytile.storage_ptr(), myarray, sizeof(T)*dim1*dim2 );

      vector<int> owner_ranks;

      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);

      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {

        remote_tile.copy(GlobalArray[*it]);

        upcxx_foreach( pt, RD(p0,p1) * RowsCols_coords[*it] ) {
          remote_tile[pt] += alpha * mytile[pt];
          //cout << "pt: " << pt << endl;
        };

        (GlobalArray[*it]/*.get()*/).async_copy(remote_tile);
      }
      async_wait();

      return;
    }


    void acc_async(point<ndim> const &p0, point<ndim> const &p1, T *myarray, \
                   void (*user_task)( ndarray<T,ndim,global>, ndarray<T,ndim,global> ) ) {

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);


      //int dim1 = p1.x[0] - p0.x[0];
      int dim2 = p1.x[1] - p0.x[1];
      rectdomain<2> tile_rd = RD( p0, p1 );
      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {

        rectdomain<2> myrd = tile_rd * RowsCols_coords[*it]; 
        if ( domain_check(myrd) ) { /* fixes bug in get_owner_ranks_irreg() */
          //cout << "this_rank " << myrank() << " says data is on " << *it << " with domain " << myrd << endl;
          ndarray<T,ndim,global> mytile ( myrd );
          upcxx_foreach( pt, myrd ) {
            mytile[pt] = myarray[(pt.x[0]-p0.x[0])*dim2 + pt.x[1] - p0.x[1]];
          };
          async( *it )( user_task, this->GlobalArray[*it], mytile );
        }
      }

      return;
    }
    
    void acc_async_ld(point<ndim> const &p0, point<ndim> const &p1, T *myarray, int const &ld, \
                      void (*user_task)( ndarray<T,ndim,global>, ndarray<T,ndim,global> ) ) {

      vector<int> owner_ranks;
      if ( irregular )
        get_owner_ranks_irreg(p0, p1, owner_ranks);
      else
        get_owner_ranks(p0, p1, owner_ranks);


      //int dim1 = p1.x[0] - p0.x[0];
      //int dim2 = p1.x[1] - p0.x[1];

      point<2> ld_pt = PT( p1.x[0], p0.x[1] + ld ); 

      rectdomain<2> tile_rd = RD( p0, ld_pt );
      
      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {

        rectdomain<2> myrd = tile_rd * RowsCols_coords[*it]; 
        if ( domain_check(myrd) ) { /* fixes bug in get_owner_ranks_irreg() */
          //cout << "this_rank " << myrank() << " says data is on " << *it << " with domain " << myrd << endl;
          ndarray<T,ndim,global> mytile ( myrd );
          upcxx_foreach( pt, myrd ) {
            mytile[pt] = myarray[(pt.x[0]-p0.x[0])*ld + pt.x[1] - p0.x[1]];
          };
          async( *it )( user_task, this->GlobalArray[*it], mytile );
        }
      }

      return;
    }

    /* Collective fill with a value */
    void fill(T const &val) {

      upcxx_foreach( pt, rd ) {
        local_array[pt] = val;
      };
      GlobalArray.exchange( local_array );
      barrier();
      return;

    }

    void transpose_elemwise() {

      barrier();
      if ( myrank() == 0 ) {
        ndarray<T, ndim> larray( global_rd );
        for (int i=0; i<ranks(); i++) {
          larray.async_copy(GlobalArray[i]/*.get()*/);
        }
        async_wait();

        upcxx_foreach( pt, global_rd ) {
          if (pt.x[0] > pt.x[1]) {
            T tmp = larray[pt];
            T a = larray[PT(pt.x[1], pt.x[0])];
            larray[pt] = a;
            larray[PT(pt.x[1], pt.x[0])] = tmp;
          }
        };
          
        for (int i=0; i<ranks(); i++) {
          (GlobalArray[i]/*.get()*/).async_copy(larray);
        }
        async_wait();
      }
      barrier();

      return;
    }

    void transpose() {

      /* Local transpose */
      rectdomain<ndim> permuted_domain = local_array.domain().transpose();
      ndarray<T, ndim> larray_T( permuted_domain );
      larray_T.copy(local_array.transpose());

#ifdef DEBUG
      //upcxx_foreach( pt, RD(lowpt_T, highpt_T) ) {
      //  cout << "pt: " << pt << ", ";
      //  larray_T[pt] = local_array[ PT(pt.x[1], pt.x[0]) ];
      //}; cout << endl;

      sleep(myrank());
      cout << "Local rectdomain: " << rd << endl;
      cout << "transposed domain: " << permuted_domain << endl;
      cout << "transposed low: " << lowpt_T << endl;
      cout << "transposed high: " << highpt_T << endl;
      cout << "DELEMS on rank:" << myrank();
      upcxx_foreach(pt, rd) {
        if (( pt.x[1]-(lowpt.x[1]) ) % (highpt.x[1]+1 - lowpt.x[1]) == 0) cout << endl;
        cout << local_array[pt] << ", ";
      };
      cout << endl << "Tranposed:";
      upcxx_foreach(pt, RD(lowpt_T, highpt_T) ) {
        if (( pt.x[1]-(lowpt_T.x[1]) ) % (highpt_T.x[1]+1 - lowpt_T.x[1]) == 0) cout << endl;
        cout << larray_T[pt] << ", ";
      };
      cout << endl << endl;
#endif

      vector<int> owner_ranks;
      point<ndim> lowpt_T = permuted_domain.min();
      point<ndim> highpt_T = permuted_domain.max()+PT(1,1);
      if ( irregular )
        get_owner_ranks_irreg(lowpt_T, highpt_T, owner_ranks);
      else
        get_owner_ranks(lowpt_T, highpt_T, owner_ranks);

      barrier();
      for (vector<int>::const_iterator it=owner_ranks.begin(); 
                                       it<owner_ranks.end(); it++) {
        (GlobalArray[*it]/*.get()*/).async_copy(larray_T);
      }
      async_wait();
      barrier();

      return;
    }

    void transpose(DArray<T,ndim> &D) {

      barrier();
      if ( myrank() == 0 ) {
        //ndarray<T, ndim> larray( global_rd );
        for (int i=0; i<ranks(); i++) {
          local_array.async_copy(D.GlobalArray[i]/*.get()*/);
        }
        async_wait();

        //upcxx_foreach( pt, global_rd ) {
        //  if (pt.x[0] > pt.x[1]) {
        //    T tmp = larray[pt];
        //    T a = larray[PT(pt.x[1], pt.x[0])];
        //    larray[pt] = a;
        //    larray[PT(pt.x[1], pt.x[0])] = tmp;
        //  }
        //};
        //  
        //for (int i=0; i<ranks(); i++) {
        //  (GlobalArray[i]/*.get()*/).async_copy(larray);
        //}
        //async_wait();
      }
      barrier();

      return;
    }

    //void symmetrize() {

    //  barrier();

    //  this->transpose();

    //  DArray<T,ndim> C( *this ) ;
    //  //DArray_add_matching( 1.0, this, 1.0, this, C );

    //  *this = C;
    //  barrier();

    //  return;
    //}

    void print(bool row_major=true) {
      barrier();


      if ( myrank() == 0 ) {

        ndarray<T, ndim> larray( global_rd );
        for (int i=0; i<ranks(); i++) {
          larray.async_copy(GlobalArray[i]/*.get()*/);
        }
        async_wait();

        int columns;
        if ( row_major ) { /* row major layout */

          col_elems < 6 ? columns = col_elems : columns = 6;

          int nchunks = col_elems / columns;
          int nleftover = col_elems % columns;
          int row, col;
          for (int i=0; i<nchunks; i++) {
            col = i*columns+1;
            row = 1;
            for (int c=0; c<columns; c++) {
              cout.width(12);
              cout << col++;
            } cout << endl << string(6, ' ');
            for (int c=0; c<columns; c++) {
              cout << " -----------";
            } cout << endl;
            col -= columns+1;
            row -= 1;
            for (int j=0; j<row_elems; j++) {
              cout.width(4);
              cout << fixed << showpoint << setprecision(5) << row+1 << "  ";
              for (int k=0; k<columns; k++) {
                cout.width(12);
                cout << fixed << showpoint << setprecision(5) << \
                        larray[PT(row, col++)];
              }
              cout << endl;
              row++;
              col-=columns;
            }
            cout << endl;
          }

          if (nleftover > 0) {
            col = nchunks*6+1;
            for (int i=0; i<nleftover; i++) {
              cout.width(12);
              cout << col++;
            } cout << endl << string(6, ' ');
            for (int c=0; c<nleftover; c++) {
              cout << " -----------";
            } cout << endl;
            col -= nleftover+1;
            row = 0;
            for (int j=0; j<row_elems; j++) {
              cout.width(4);
              cout << fixed << showpoint << setprecision(5) << row+1 << "  ";
              for (int k=0; k<nleftover; k++) {
                cout.width(12);
                cout << fixed << showpoint << setprecision(5) << \
                        larray[PT(row, col++)];
              }
              cout << endl;
              row++;
              col-=nleftover;
            }
          }

        } else { /* column major layout */

          row_elems < 6 ? columns = row_elems : columns = 6;

          int nchunks = row_elems / columns;
          int nleftover = row_elems % columns;
          int row, col;
          for (int i=0; i<nchunks; i++) {
            row = i*columns+1;
            col = 1;
            for (int c=0; c<columns; c++) {
              cout.width(12);
              cout << row++;
            } cout << endl << string(6, ' ');
            for (int c=0; c<columns; c++) {
              cout << " -----------";
            } cout << endl;
            row -= columns+1;
            col -= 1;
            for (int j=0; j<col_elems; j++) {
              cout.width(4);
              cout << fixed << showpoint << setprecision(5) << col+1 << "  ";
              for (int k=0; k<columns; k++) {
                cout.width(12);
                cout << fixed << showpoint << setprecision(5) << \
                        larray[PT(row++, col)];
              }
              cout << endl;
              col++;
              row-=columns;
            }
            cout << endl;
          }

          if (nleftover > 0) {
            row = nchunks*6+1;
            for (int i=0; i<nleftover; i++) {
              cout.width(12);
              cout << row++;
            } cout << endl << string(6, ' ');
            for (int c=0; c<nleftover; c++) {
              cout << " -----------";
            } cout << endl;
            row -= nleftover+1;
            col = 0;
            for (int j=0; j<col_elems; j++) {
              cout.width(4);
              cout << fixed << showpoint << setprecision(5) << col+1 << "  ";
              for (int k=0; k<nleftover; k++) {
                cout.width(12);
                cout << fixed << showpoint << setprecision(5) << \
                        larray[PT(row++, col)];
              }
              cout << endl;
              col++;
              row-=nleftover;
            }
          }
        }
      }
      barrier();
      return;
    }

};

#endif
