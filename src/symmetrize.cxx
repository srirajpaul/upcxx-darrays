#include <upcxx/timer.h>
#include "symmetrize.h"
#include "add.h"
using namespace upcxx;
using namespace std;

#define TESTRANK 0
//#define TIMERS

void DArray_symmetrize( DArray<double,2> &A ) {
#ifdef DEBUG
  if (myrank() == 0) cout << "A:" << endl; 
  A.print();
#endif

#ifdef TIMERS
  timer timer1, timer2, timer3;
  timer1.start();
#endif
  //DArray<double,2> B ( A );
  //DArray<double,2> C ( A );


  double *ptr = A.get_local_block_ptr();
  ndarray<double,2> myA_block = A.get_local_block();
  point<2> lowpt = myA_block.domain().min();
  point<2> highpt = myA_block.domain().max() + PT(1,1);
#ifdef DEBUG
  if (myrank() == TESTRANK) cout << "highpt: " << highpt << endl;
  if (myrank() == TESTRANK) cout << "lowpt: " << lowpt << endl;
#endif
  ndarray<double,2> myB_block( RD(lowpt, highpt) );
  //int dim1 = highpt.x[0] - lowpt.x[0];
  //int dim2 = highpt.x[1] - lowpt.x[1];
  //cout << "dim1= " << dim1 << endl;
  //cout << "dim2= " << dim2 << endl;
  //memcpy(myB_block.storage_ptr(), ptr, sizeof(double)*dim1*dim2 );

  upcxx_foreach( pt, myA_block.domain() ) {
    myB_block[pt] = myA_block[pt];
  };

#ifdef DEBUG
  if (myrank() == TESTRANK) cout << "myB_block[0,2] = " << myB_block[PT(0,2)] << endl;
  if (myrank() == TESTRANK) cout << "myB_block[1,3] = " << myB_block[PT(1,3)] << endl;
#endif

#ifdef TIMERS
  timer1.stop();
  if (myrank() == 0)
    printf("create copies takes %.3f secs\n", timer1.secs());

  timer2.start();
#endif

  A.transpose();

#ifdef TIMERS
  timer2.stop();
  if (myrank() == 0)
    printf("transpose takes %.3f secs\n", timer2.secs());
#endif
//#ifdef DEBUG
//  if (myrank() == 0) cout << "B:" << endl; 
//  B.print();
//#endif

#ifdef DEBUG
  if (myrank() == TESTRANK) cout << "myB_block[0,2] = " << myB_block[PT(0,2)] << endl;
  if (myrank() == TESTRANK) cout << "myA_block[0,2] = " << myA_block[PT(0,2)] << endl;
  if (myrank() == TESTRANK) cout << "myB_block[1,3] = " << myB_block[PT(1,3)] << endl;
  if (myrank() == TESTRANK) cout << "myA_block[1,3] = " << myA_block[PT(1,3)] << endl;
#endif

#ifdef TIMERS
  timer3.start();
#endif
  
  //DArray_add_matching( 0.5, A, 0.5, A, A );
  upcxx_foreach( pt, myA_block.domain() ) {
#ifdef DEBUG
    cout << "myA[" << pt << "] = " << myA_block[pt] << endl;
#endif
    myA_block[pt] = 0.5 * myB_block[pt] + 0.5 * myA_block[pt];
  };

#ifdef TIMERS
  timer3.stop();
  if (myrank() == 0)
    printf("add_matchin takes %.3f secs\n", timer3.secs());
  timer3.reset();
  timer3.start();
#endif

  //A = C;
  A.update();

#ifdef TIMERS
  timer3.stop();
  if (myrank() == 0)
    printf("A.update() takes %.3f secs\n", timer3.secs());
#endif

#ifdef DEBUG
  if (myrank() == 0) cout << "C:" << endl; 
  C.print();
#endif

#ifdef DEBUG
  if (myrank() == 0) cout << "new A:" << endl; 
  A.print();
#endif

  barrier();

  return;
}
