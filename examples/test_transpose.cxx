#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 2;
  const int ncols = 3;
  // TODO: const int nelems = 54;
  const int nelems = 30;

  int block_rows[nrows+1] = {0, 5, 30};
  int block_cols[ncols+1] = {0, 10, 15, 30};

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( block_rows[0], block_cols[0] ),\
                  PT( block_rows[nrows], block_cols[ncols] ) );

  local_rd  = RD( PT( block_rows[  myrank()/ncols  ],   \
                      block_cols[  myrank()%ncols  ] ), \
                  PT( block_rows[ myrank()/ncols+1 ],   \
                      block_cols[ myrank()%ncols+1 ] ) );

  myblock.create( local_rd );

  DArray<double, ndims> D(nrows, ncols, global_rd, myblock);


  if (myrank() == 2) {
    cout << "elements per row: " << D.get_elems_per_row() << endl;
    cout << "elements per col: " << D.get_elems_per_col() << endl;
    cout << "my row, col = " << D.get_myrow() << ", " << D.get_mycol() << endl;
    cout << "[low,high] = [" << D.get_lowpt() << ", " << D.get_highpt() << "]\n";

    cout << "my lowpt data = " << D.get_local_block()[D.get_lowpt()] << endl;
    cout << "my highpt data = " << D.get_local_block()[D.get_highpt()] << endl;


    //ndarray<double, ndims, local> tile;
    //point<2> upper_left  = PT(4,4); 
    //point<2> lower_right = PT(14,14);
    //rectdomain<2> rd = RD( upper_left, lower_right );

    //tile.create( rd );
    //upcxx_foreach( pt, rd ) {
    //  tile[pt] = 7;
    //};

    //D.put_tile( tile );

    double *A = (double *)_mm_malloc(5*5* sizeof (double), 64);
    for (int i=0; i<5; i++) {
      for (int j=0; j<5; j++) {
        A[i*5+j] = j;
      }
    }
    point<2> upper_left  = PT(4,4); 
    point<2> lower_right = PT(9,9);
    D.put_tile( upper_left, lower_right, A, 5 );


  }

  /* Assure tile has been written to the darray */
  barrier();

  if (myrank() == 3) {

    ndarray<double, ndims> tile;
    double *Z = (double *)_mm_malloc(9*9* sizeof (double), 64);

    point<2> upper_left  = PT(2,2);
    point<2> lower_right = PT(11,11);

    rectdomain<2> rd = RD( upper_left, lower_right );

    //tile = D.get_tile_fg( upper_left, lower_right );
    //tile.create( rd ); D.get_tile( &tile );
    //tile = D( upper_left, lower_right );
    D.get_storage( upper_left, lower_right, Z );

    //cout << "delems:";
    //upcxx_foreach(pt, rd ) {
    //  if (( pt.x[1]-(upper_left.x[1]) ) % \
    //      (lower_right.x[1] - upper_left.x[1]) == 0) cout << endl;
    //  cout << tile[pt] << ", ";
    //};
    //cout << endl;
    
    cout << "delems:" << endl;
    for (int i = 0; i<9; i++) {
      for (int j = 0; j<9; j++) {
        cout << Z[i*9 + j] << ", ";
      }cout << endl;
    }  


  }

  barrier();

  D.print(false);

  //D.transpose();

  DArray<double, 2> E ( D );
  E.transpose(D);

  E.print(false);

  /* Finalize UPC++ */
  finalize();

  return 0;

}
