#include <upcxx.h>
#include <upcxx/array.h>
#include <upcxx/timer.h>
#include <darray.h>
#include <symmetrize.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  if (argc != 4) {
    if ( myrank() == 0 ) cerr << "Need 3 arguments" << endl;
    finalize();
  }

  const int ndims = 2;
  const int nrows = atoi( argv[1] );
  const int ncols = atoi( argv[2] );
  const int nelems = atoi( argv[3] );
  if ( myrank() == 0 ) {
    cout << "nrows: " << nrows << endl;
    cout << "ncols: " << ncols << endl;
    cout << "nelems: " << nelems << endl;
  }

  if (ranks() != nrows * ncols ) {
    if ( myrank() == 0 ) cerr << "(nrows * ncols) must be equal to ranks()" << endl;
    finalize();
  }

  int block_rows[nrows+1];
  int block_cols[ncols+1];
  int ave_elems_per_row = nelems/nrows;
  int ave_elems_per_col = nelems/ncols;

  int row_idx = 0;
  int col_idx = 0;
  for( int i=0; i<nrows; i++ ) {
    block_rows[i] = row_idx;
    row_idx += ave_elems_per_row;
  }
  block_rows[nrows] = nelems;
  for( int i=0; i<ncols; i++ ) {
    block_cols[i] = col_idx;
    col_idx += ave_elems_per_col;
  }
  block_cols[ncols] = nelems;

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( block_rows[0], block_cols[0] ),\
                  PT( block_rows[nrows], block_cols[ncols] ) );

  local_rd  = RD( PT( block_rows[  myrank()/ncols  ],   \
                      block_cols[  myrank()%ncols  ] ), \
                  PT( block_rows[ myrank()/ncols+1 ],   \
                      block_cols[ myrank()%ncols+1 ] ) );

  myblock.create( local_rd );

  DArray<double, ndims> A(nrows, ncols, global_rd, myblock);

  ndarray<double, 2> putblock ( global_rd );

  double d = 1.0;
  upcxx_foreach( pt, global_rd ) {
    //putblock[PT(pt.x[1], pt.x[0])] = d;
    putblock[pt] = d;
    d += 1.0;
  };

  A.put_tile( putblock );

  //A.print(false);

  barrier();

  timer timer1;
  timer1.start();
  DArray_symmetrize( A );
  timer1.stop();

  if ( myrank() == 0 )
    cout << "UPC++ Symmetrize:           " << timer1.secs() << " seconds" << endl;

  //if (myrank() == 0) cout << endl << endl << endl;

  //A.print();

  if (myrank() == 0) cout << endl;

  /* Finalize UPC++ */
  finalize();

  return 0;

}
