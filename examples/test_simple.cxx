#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 1;
  const int ncols = 1;
  const int nelems = 11;

  DArray<double, ndims> D(nrows, ncols, nelems);

  D.fill(1.2345);

  D.print();

  /* Finalize UPC++ */
  finalize();

  return 0;

}
