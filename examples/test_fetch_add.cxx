#include <upcxx.h>
#include <upcxx/array.h>
#include <iostream>
using namespace upcxx;
using std::cout; using std::endl;

int fetch_add_loc( ndarray<int,2,global> &A, point<2> const pt, int const &val ) {
  A[pt] += val;
  return A[pt];
}

int fetch_add( shared_array<int> &A, int const idx, int const &val ) {
  A[idx] += val;
  return A[idx];
}

int main(int argc, char** argv) {

  init(&argc, &argv);

  const int nrows = 2;
  const int ncols = 3;

  static shared_array<int> SA ( nrows*ncols );

  SA[myrank()] = 0;
  //barrier();

  rectdomain<2> rd = RD( PT(0,0), PT(nrows,ncols) );
  ndarray<int, 2, global> LA ( rd );

  upcxx_foreach( pt, rd ) {
    LA[pt] = SA[pt.x[0]*ncols + pt.x[1]];
  };
  barrier();

  int ntasks = 100000;
  int task = 0;
  std::srand(0);

  while ((task = fetch_add_loc( LA, PT(rand()%nrows, rand()%ncols), 1 )) < ntasks ){
  //while ((task = fetch_add( SA, rand()%ranks(), 1 )) < ntasks ){
  //while ((task = fetch_add( SA, myrank(), 1 )) < ntasks ){
    /* do work */
  }

  barrier();
  std::cout << "FINAL VALUE: " << task << std::endl;

  finalize();

  return 0;

}
