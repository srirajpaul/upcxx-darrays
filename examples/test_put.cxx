#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 3;
  const int ncols = 4;
  // TODO: const int nelems = 54;
  const int nelems = 60;

  DArray<double, ndims> D(nrows, ncols, nelems);

  ndarray<double, ndims, local> tile;


  if (myrank() == 2) {
    cout << "elements per row: " << D.get_elems_per_row() << endl;
    cout << "elements per col: " << D.get_elems_per_col() << endl;
    cout << "my row, col = " << D.get_myrow() << ", " << D.get_mycol() << endl;
    cout << "[low,high] = [" << D.get_lowpt() << ", " << D.get_highpt() << "]\n";

    cout << "my lowpt data = " << D.get_local_block()[D.get_lowpt()] << endl;
    cout << "my highpt data = " << D.get_local_block()[D.get_highpt()] << endl;


    point<2> upper_left  = PT(4,4); 
    point<2> lower_right = PT(14,14);
    rectdomain<2> rd = RD( upper_left, lower_right );

    tile.create( rd );
    upcxx_foreach( pt, rd ) {
      tile[pt] = 7;
    };

     D.put_tile( tile );

  }

  barrier();

  if (myrank() == 3) {

    point<2> upper_left  = PT(0,0); 
    point<2> lower_right = PT(20,20);
    rectdomain<2> rd = RD( upper_left, lower_right );

    //tile = D.get_tile_fg( upper_left, lower_right );
    tile = D( upper_left, lower_right );

    cout << "delems:";
    upcxx_foreach(pt, rd ) {
      if (( pt.x[1]-(upper_left.x[1]) ) % \
          (lower_right.x[1] - upper_left.x[1]) == 0) cout << endl;
      cout << tile[pt] << ", ";
    };
    cout << endl;
  }

  //D.print();

  /* Finalize UPC++ */
  finalize();

  return 0;

}
