#include <upcxx.h>
#include <upcxx/array.h>
#include <darray.h>
#include <add.h>
#include <iostream>
using namespace std;
using namespace upcxx;

int main(int argc, char** argv) {

  /* Initialize UPC++ */
  init(&argc, &argv);

  const int ndims = 2;
  const int nrows = 2;
  const int ncols = 3;
  // TODO: const int nelems = 54;
  const int nelems = 6;

  int block_rows[nrows+1] = {0, 1, 6};
  int block_cols[ncols+1] = {0, 2, 3, 6};

  ndarray<double, ndims, local> myblock;
  rectdomain<ndims> global_rd, local_rd;

  global_rd = RD( PT( block_rows[0], block_cols[0] ),\
                  PT( block_rows[nrows], block_cols[ncols] ) );

  local_rd  = RD( PT( block_rows[  myrank()/ncols  ],   \
                      block_cols[  myrank()%ncols  ] ), \
                  PT( block_rows[ myrank()/ncols+1 ],   \
                      block_cols[ myrank()%ncols+1 ] ) );

  myblock.create( local_rd );

  DArray<double, ndims> A(nrows, ncols, global_rd, myblock);
  DArray<double, ndims> B(nrows, ncols, global_rd, myblock);
  DArray<double, ndims> C(nrows, ncols, global_rd, myblock);

  ndarray<double, 2> putblock ( global_rd );

  double d = 1.0;
  upcxx_foreach( pt, global_rd ) {
    //putblock[PT(pt.x[1], pt.x[0])] = d;
    putblock[pt] = d;
    d += 1.0;
  };

  A.put_tile( putblock );
  B.put_tile( putblock );

  //A.print();

  DArray_add_matching( 2.0, A, 3.0, B, C );

  C.print();

  /* Finalize UPC++ */
  finalize();

  return 0;

}
